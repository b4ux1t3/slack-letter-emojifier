export class Main {
  private prefixInput: HTMLInputElement;
  private postfixInput: HTMLInputElement;
  private clientRadioInputs: HTMLCollectionOf<Element>;
  private textInput: HTMLInputElement;
  private textOutput: HTMLTextAreaElement;
  private overrideKeyInput: HTMLInputElement;
  private overrideMacroInput: HTMLInputElement;
  private addButton: Element;
  private overrideSection: Element;
  
  private prefix: string;
  private postfix: string;
  private overrideKeys: string[];
  private overrides: {[character: string]: string};

  private clientDefaults: {[client: string]: {[template: string]: string}} = {
    'slack': {
      'prefix': ':alphabet-white-',
      'postfix': ':',
    },
    'discord': {
      'prefix': ':regional_indicator_',
      'postfix': ': ',
    }
  }

  private currentDefault = 'slack';

  constructor(prefix?: string, postfix?: string) {
    this.initializeElements();
    this.initializeTemplate(prefix, postfix);
    this.initializeEventListeners();
    this.initializeOverrides();
  }

  initializeElements(): void {
    this.prefixInput = document.getElementById('prefixInput') as HTMLInputElement;
    this.postfixInput = document.getElementById('postfixInput') as HTMLInputElement;
    this.clientRadioInputs = document.getElementsByClassName('client-radio');
    this.textInput = document.getElementById('input') as HTMLInputElement;
    this.textOutput = document.getElementById('output') as HTMLTextAreaElement;
    this.overrideKeyInput = document.getElementById('overrideCharacterInput') as HTMLInputElement;
    this.overrideMacroInput = document.getElementById('overrideMacroInput') as HTMLInputElement;
    this.addButton = document.getElementById('addOverride');
    this.overrideSection = document.getElementById('overrideBody');
  }

  initializeTemplate(prefix?: string, postfix?: string): void {
    if (prefix !== undefined){
      this.prefix = prefix;
    } else {
      this.prefix = ':alphabet-white-';
    }

    if (postfix !== undefined){
      this.postfix = postfix;
    } else {
      this.postfix = ':';
    }

    this.prefixInput.value = this.prefix;
    this.postfixInput.value = this.postfix;
  }

  setTemplate(prefix: string, postfix: string): void {
    this.prefix = prefix;
    this.postfix = postfix;
  }

  initializeEventListeners(): void {
    // Add our Event Listeners
    this.textInput.addEventListener('input', (e: Event) => { 
      this.textOutput.value = this.parseCharacters();
    });
    this.addButton.addEventListener('click', this.addOverrideHandler.bind(this));
    this.prefixInput.addEventListener('input', (e: Event) => this.prefix = this.prefixInput.value); 
    this.postfixInput.addEventListener('input', (e: Event) => this.postfix = this.postfixInput.value); 
    for (let i = 0; i < this.clientRadioInputs.length; i++) {
      this.clientRadioInputs.item(i).addEventListener('click', (e: Event) => {
        console.log(e);
        this.currentDefault = (document.querySelector('input[type = radio]:checked') as HTMLInputElement).value;
        this.initializeTemplate(this.clientDefaults[this.currentDefault]['prefix'], this.clientDefaults[this.currentDefault]['postfix']);

      })
    }
  }

  initializeOverrides(): void {
    this.overrides = {};

    this.overrideKeys = [];
  }
  
  parseCharacters(): string{
    let sb = ""; // StringBuilder pattern. . .ish
    this.textInput.value.split("").forEach((character) => {
      if (this.overrideKeys.includes(character)) {
        sb += this.overrides[character];
      } else if (character.match(/[a-zA-Z]/)){
        sb += `${this.prefix}${character.toLocaleLowerCase()}${this.postfix}`;
      } else {
        sb += character;
      }
    });
    
    return sb;
  }

  addOverride(character: string, emojiMacro: string): void {
    this.overrides[character] = emojiMacro;
    if (!this.overrideKeys.includes(character))this.overrideKeys.push(character);
    // make a new key div
    const newKey = document.createElement('td');
    newKey.innerText = character;
    newKey.classList.add('overrideKey');
    
    // create a new macro div
    const newMacro = document.createElement('td');
    newMacro.innerText = this.overrides[character];
    newMacro.classList.add('overrideMacro');
    
    // create a button
    const newButtonCell = document.createElement('td');
    const newButton = document.createElement('button');
    newButton.innerText = '-';
    newButton.classList.add('btn', 'btn-danger');
    newButton.id = `remove-${character}`;

    newButton.addEventListener('click', (e: Event) => {
      this.overrideSection.removeChild(document.getElementById(`override-${character}-row`));
      this.overrideKeys.splice(this.overrideKeys.findIndex((element: string) => element === character), 1);
      delete this.overrides[character];
    });

    newButtonCell.appendChild(newButton);

    // create a new row
    const newRow = document.createElement('tr');
    newRow.classList.add('overrideRow');
    newRow.id = `override-${character}-row`
    
    newRow.appendChild(newKey);
    newRow.appendChild(newMacro);
    newRow.appendChild(newButtonCell);
    
    this.overrideSection.appendChild(newRow);
  }

  addOverrideHandler(e: Event): void {
    const keyInputValidation = this.overrideKeyInput.value.length === 1;
    const macroInputValidation = this.overrideMacroInput.value.length > 0;
    if (keyInputValidation && macroInputValidation){
      this.addOverride(this.overrideKeyInput.value, this.overrideMacroInput.value);
      this.overrideKeyInput.value = '';
      this.overrideMacroInput.value ='';
    } else {
      console.log('form not valid');
    }
  }
}
