FROM node:latest as build

WORKDIR /build
COPY ./*.json .

RUN npm install

COPY ./public ./public
COPY ./src ./src

COPY *.js .

RUN npm run build-prod

FROM nginx:latest

COPY --from=build /build/dist/main.js /usr/share/nginx/html/
COPY --from=build /build/public/index.html /usr/share/nginx/html/

