# Slack Letter Emojifier
This little webpage will turn any given text into Slack's (or other chat clients') emoji letters.

To use it, you just need to enter some text into the input, then copy/paste the output into Slack (or your other chat client).

Your Slack instance (or other chat client) will have to have some kind of emoji alphabet pack installed. If yours does not match the pattern `:alphabet-white-[a-z]:`, you can define a custom prefix and postfix at the top of the app.

The app supports Slack's (at least my Slack's) and Discord's emoji letters by default.

## How to Run
There are a couple ways to run the app. 

1. Access the app directly from [GitLab Pages](https://b4ux1t3.gitlab.io/slack-letter-emojifier/)

2. There is an NPM script for serving the page with the `webpack-dev-server`. 

3. If you don't have Node installed on your system, but you *do* have docker installed, you can build and run the container with this one-liner:

``` bash
docker build -t b4ux1t3/slack-emojifier && docker run --rm -d -p 80:80 b4ux1t3/slack-emojifier
```

This will run the container with port 80 exposed.


